---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Inicio
authors:
- Andrés Leonardo Hernández Bermúdez
---

![Pipeline Status](https://gitlab.com/Redes-Ciencias-UNAM/2022-2/tareas-redes/badges/master/pipeline.svg)

# Repositorio de actividades - Redes - 2022-2

En este repositorio estaremos manejando las tareas de la materia de [Redes][gitlab] que se imparte en la Facultad de Ciencias, UNAM en el [semestre 2022-2][horario]

## Entregas

- Las entregas de las tareas y prácticas se realizan **en equipo** de `3` o `4` personas
- **No se aceptan trabajos individuales**. Esto quedo definido en la [presentación del curso][presentacion]

## Organización de las entregas

Cada entrega debe cumplir con los siguientes lineamientos:

- Levantar un [_merge request_][merge-requests] para entregar la actividad
    - El _flujo de trabajo_ viene documentado en [la carpeta `workflow`](./workflow)
- Cada **equipo** entregará sus tareas y prácticas dentro de la carpeta correspondiente
- Cada tarea o práctica deberá tener lo siguiente:
    - Un archivo `README.md` donde se entregará la documentación o reporte en formato [_markdown_][guia-markdown]
    - Se puede hacer uso de un directorio `img` para guardar las imágenes o capturas de pantalla necesarias para la documentación
    - Una carpeta llamada `files` para incluir los archivos adicionales de la entrega en caso de ser necesario

### Estructura de directorios

La estructura de directorios que se manejará para las entregas es la siguiente:

- Una carpeta principal llamada `tareas` o `practicas`
    - Una carpeta por cada tarea (`tareas/tarea-1`) o práctica (`practicas/practica-2`)
        - No utilizar espacios, acentos ni caracteres especiales en ningún nombre de archivo o carpeta
        - Cada carpeta de tarea o práctica tendrá un archivo `README.md` donde se especifica lo que se deberá entregar
        - Cada equipo creará una carpeta llamada `Equipo-AAAA-BBBB-CCCC-DDDD`, donde se realizará la entrega
            - `AAAA`, `BBBB`, `CCCC` y `DDDD` son las iniciales de los integrantes del equipo

```
docs/
├── tareas/
│   ├── tarea-0/
│   │   ├── AndresHernandez/
│   │   │   ├── img/
│   │   │   │   └── ...
│   │   │   └── README.md
│   │   ├── JoseAntonioMartinezBalderas/
│   │   │   └── ...
│   │   └── JoseLuisTorresRodriguez/
│   │       └── ...
│   ├── tarea-1/
│   │   ├── Equipo-AAAA-BBBB-CCCC-DDDD/
│   │   │   ├── img/
│   │   │   │   └── ...
│   │   │   ├── files/
│   │   │   │   └── ...
│   │   │   └── README.md
│   │   └── Equipo-WWWW-XXXX-YYYY-ZZZZ/
│   │       └── ...
│   └── tarea-2/
│       └── ...
└── practicas/
    ├── practica-1/
    │   ├── Equipo-AAAA-BBBB-CCCC-DDDD/
    │   │   ├── img/
    │   │   │   └── ...
    │   │   ├── files/
    │   │   │   └── ...
    │   │   └── README.md
    │   └── Equipo-WWWW-XXXX-YYYY-ZZZZ/
    │       └── ...
    └── practica-2/
        └── ...
```

<!--
--------------------------------------------------------------------------------

- [Vista en GitLab](https://gitlab.com/Redes-Ciencias-UNAM/2022-2/tareas-redes)
- [Vista web](https://Redes-Ciencias-UNAM.gitlab.io/2022-2/tareas-redes)
-->

--------------------------------------------------------------------------------

[gitlab]: https://Redes-Ciencias-UNAM.gitlab.io/
[horario]: https://www.fciencias.unam.mx/docencia/horarios/presentacion/332404
[presentacion]: https://redes-ciencias-unam.gitlab.io/curso/
[guia-markdown]: https://about.gitlab.com/handbook/markdown-guide/
[merge-requests]: https://gitlab.com/Redes-Ciencias-UNAM/2022-2/tareas-redes/-/merge_requests
